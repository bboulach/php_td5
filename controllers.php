<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            in_array(array($post->get('login'), $post->get('password')), $app['config']['admin'])) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('home'));
})->bind('logout');


$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');

//////////added functions //////////////////


/*$app->match('/login', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            in_array(array($post->get('login'), $post->get('password')), $app['config']['admin'])) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    return $app['twig']->render('login.html.twig', array(
        'success' => $success
    ));
})->bind('login');*/



$app->match('/book/{id}', function($id) use ($app) {
    $book = $app['model']->getBook($id)[0];
    $nbBook = $app['model']->getNbBook($id);
    $loanBook = $app['model']->getAllLoanBook($nbBook);
    
   


    //$returnBook = $app['model']->setReturnBook($id);
    return $app['twig']->render('book.html.twig', array(
        'book' => $book,
        'nbBook' => $nbBook,
        'loanBook' => $loanBook,
        //'returnBook' =>$returnBook
    ));
})->bind('book');


$app->match('/loanBook/{id}', function($id) use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    $success = false;
    $dateSuccess =true;
    


    if ($request->getMethod() == 'POST') {
        $post = $request->request;

        $nowDate = date('dmY');
        $endDate = new Datetime($_POST['end_date']);
        $endDate = $endDate->format('dmY');
        if ($post->has('name') && $post->has('end_date') && ($nowDate < $endDate)){
            $app['model']->loanBook($post->get('name'), date('d-m-Y-h-m'), $post->get('end_date'), $id);
            $success = true;
        }
        else
            $dateSuccess = false;

    }

    if($success){
        return $app['twig']->render('home.html.twig', array(
            'success' => $success));
    }

    return $app['twig']->render('loanBook.html.twig', array(
        'dateSuccess' => $dateSuccess
        ));

})->bind('loanBook');




$app->match('/returnBook/{id}', function($id) use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }


    $request = $app['request'];
    $success2 = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('returnBook') && $_POST['returnBook'] =="yes" ){
            $returnBook = $app['model']->setReturnBook($id);
            $success2 = true;
        }
        else
            return $app->redirect('/books');
    }

    if($success2){
        return $app['twig']->render('home.html.twig', array(
        'success2' => $success2));
    }

    return $app['twig']->render('returnBook.html.twig');
})->bind('returnBook');

//     $returnBook = $app['model']->setReturnBook($id);
//     return $app->redirect('/books');
// })->bind('returnBook');

