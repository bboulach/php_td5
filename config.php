<?php

return [
    // Database
    'database' => [
        'engine' => 'mysql',
        'host' => 'localhost',
        'database' => 'library',
        'user' => 'root1',
        'password' => 'root'
    ],

    // Administrator auth
    'admin' => [['admin', 'password'], 
                ['admin2', 'password2'], 
                ['admin3', 'password3']]

];
