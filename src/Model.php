<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                    );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                    );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        
        // TODO: Créer $copies exemplaires
        $query2 = $this->pdo->prepare('INSERT INTO exemplaires (book_id)
            VALUES (?)');
        //On retourne l'identifiant de la dernière ligne insérée 
        $copie_book_id = $this->pdo->lastInsertId();
        for ($i= 0; $i<$copies; $i++) {
            $this->execute($query2, array($copie_book_id));
        }
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }


    /**
     * Getting a book
     */
    public function getBook($id)
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres WHERE livres.id= :id');

        //PDOStatement::execute
        $this->execute($query, array(":id" => $id));

        return $query->fetchAll();
    }


     /**
     * Getting number copies of a book.
     */
     public function getNbBook($id)
     {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires WHERE exemplaires.book_id = :id');

        $this->execute($query, array(":id" => $id));

        return $query->fetchAll();
    }

    /**
     * Getting number copies of a book. Without id for tests
     */
     public function getNbBooks()
     {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires');

        $this->execute($query);

        return $query->fetchAll();
    }



    public function loanBook($nom, $debut, $fin, $id)
    {
       $query = $this->pdo->prepare('INSERT INTO emprunts (personne, debut, fin, exemplaire) 
        VALUES (?, ?, ?, ?)');
       $this->execute($query, array($nom, $debut, $fin, $id));

        //return $query->fetchAll();
   }


   /*public function getAllLoanBook($id)
   {
     $query = $this->pdo->prepare('SELECT * FROM emprunts
         JOIN exemplaires ON exemplaires.book_id = emprunts.exemplaire 
         WHERE exemplaires.book_id=:id');
     $this->execute($query,array(":id" => $id)) ;
     return $query->fetchAll();
 }*/

 public function getAllLoanBook($nbBook)
 {


     $query = $this->pdo->prepare('SELECT * FROM emprunts WHERE fini = 0');
     $this->execute($query) ;
     $allLoanBook = $query->fetchAll();
     $arrayLoan = array();


     //On place dans arrayLoan tous les livres empruntés parmis les exemplaires
     foreach ($allLoanBook as $loanBook) {
        foreach ($nbBook as $book){
            if ($book['id'] == $loanBook['exemplaire']){
                array_push($arrayLoan, $loanBook);
            }
        }
    }
    //var_dump($arrayLoan);
    return $arrayLoan;
}

//For all the books
public function getAllLoanBooks()
{
    $query = $this->pdo->prepare('SELECT * FROM emprunts WHERE fini = 0');
    $this->execute($query) ;
    $allLoanBook = $query->fetchAll();
}

    


//Set fini = 1 when a book is returned
public function setReturnBook($id)
 {
    $query = $this->pdo->prepare('UPDATE emprunts SET fini = 1 WHERE id = :id');
    $this->execute($query, array(":id" => $id));


 }



}
